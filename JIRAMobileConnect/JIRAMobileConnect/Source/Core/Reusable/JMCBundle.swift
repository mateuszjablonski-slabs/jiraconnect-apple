//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

class Bundle: NSBundle {
  static let JIRAMobileConnectBundle = NSBundle(forClass: Bundle.self)
}
