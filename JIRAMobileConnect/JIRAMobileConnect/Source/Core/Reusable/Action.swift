//  Copyright (c) 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

class Action: NSOperation {
  override func main() {
    if cancelled {
      return
    }
    autoreleasepool {
      self.run()
    }
  }
  
  func run() {
    preconditionFailure("This abstract method must be overridden.")
  }
}
