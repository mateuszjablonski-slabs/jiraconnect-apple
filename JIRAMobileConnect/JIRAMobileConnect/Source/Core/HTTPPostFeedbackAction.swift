//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation
import Alamofire

let alamofireManager = Alamofire.Manager(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())

public class HTTPPostFeedbackAction: AsyncAction {
  let issue: Issue
  let screenshotImageOrNil: UIImage?
  let target: JMCTarget
  let onComplete: Outcome<Void>->Void
  var issueData: NSData!
  
  var issueJSONInfo: [String: NSObject] {
    // Any other interesting pieces of data to grab?
    let issueInfo = issue.infoAsDictionary
    let deviceInfo = UIDevice.currentDevice().infoAsDictionary
    let languageInfo = NSLocale.infoAsDictionary
    let bundleInfo = NSBundle.mainBundle().infoAsDictionary
    return [issueInfo, deviceInfo, languageInfo, bundleInfo].reduce([:], combine: +)
  }
  
  var screenshotImageDataOrNil: NSData? {
    switch screenshotImageOrNil {
    case .Some(let screenshotImage):
      return UIImageJPEGRepresentation(screenshotImage, 1.0)
    default:
      return nil
    }
  }
  
  public init(issue: Issue, screenshotImageOrNil: UIImage? = nil, target: JMCTarget, onComplete: Outcome<Void>->Void) {
    self.issue = issue
    self.screenshotImageOrNil = screenshotImageOrNil
    self.target = target
    self.onComplete = onComplete
    super.init()
  }
  
  override public func run() {
    // IMPORTANT: Encoding memory threshold is 10 mb by default.
    do {
      issueData = try serializeIssueData()
    } catch {
      finishedExecutingOperationWithOutcome(.Error(error))
      return
    }
    
    alamofireManager.startRequestsImmediately = true
    alamofireManager.upload(
      .POST,
      target.postIssueURLString,
      headers: ["-x-jmc-requestid":UIDevice.currentDevice().identifierForVendorString],
      multipartFormData: appendToMultipartFormData,
      encodingCompletion: onMultipartEncodingComplete)
  }
  
  func appendToMultipartFormData(multipartFormData: MultipartFormData) {
    multipartFormData.appendBodyPart(data: issueData, name: "issue", fileName: "issue.json", mimeType: "application/json")
    
    if let screenshotImageData = screenshotImageDataOrNil {
      multipartFormData.appendBodyPart(data: screenshotImageData, name: "screenshot", fileName: "screenshot.jpg", mimeType: "image/jpeg")
    }
  }
  
  func onMultipartEncodingComplete(encodingResult: Manager.MultipartFormDataEncodingResult) {
    switch encodingResult {
    case .Success(let upload, _, _):
      upload.responseJSON(completionHandler: onUploadComplete)
    case .Failure(let encodingError):
      finishedExecutingOperationWithOutcome(.Error(encodingError))
    }
  }
  
  func onUploadComplete(requestOrNil: NSURLRequest?, responseOrNil: NSHTTPURLResponse?, result: Result<AnyObject>) {
    switch result {
    case .Success:
      onHTTPResponse(responseOrNil!)
    case .Failure(_, let error):
      finishedExecutingOperationWithOutcome(.Error(error))
    }
  }
  
  // For Alamofire 3:
//  func onUploadComplete(response: Response<AnyObject, NSError>) {
//    switch response.result {
//    case .Success:
//      onHTTPResponse(response.response)
//    case .Failure(let error):
//      finishedExecutingOperationWithOutcome(.Error(error))
//    }
//  }
  
  func onHTTPResponse(HTTPResponseOrNil: NSHTTPURLResponse?) {
    guard let HTTPResponse = HTTPResponseOrNil else {
      finishedExecutingOperationWithOutcome(.Error(JMCError.NilHTTPResponseError))
      return
    }
    
    switch  HTTPResponse.statusCode {
    case let statusCode where statusCode.isSuccessHTTPStatuCode:
      finishedExecutingOperationWithOutcome(.Success())
    default:
      finishedExecutingOperationWithOutcome(.Error(JMCError.HTTPResponseError))
    }
  }
  
  func serializeIssueData() throws -> NSData {
    return try NSJSONSerialization.dataWithJSONObject(issueJSONInfo, options: [])
  }
  
  func finishedExecutingOperationWithOutcome(outcome: Outcome<Void>) {
    finishedExecutingOperation()
    onComplete(outcome)
  }
}

extension JMCTarget {
  var postIssueURLString: String {
    return "https://" + host + "/" + "rest/jconnect/" + "1.0" + "/issue/create?" + "project=" + projectKey + "&apikey=" + apiKey
  }
}

extension Int {
  private var isSuccessHTTPStatuCode: Bool {
    return 200...299 ~= self
  }
}

// Add function for combining dictionaries.
private func + <K, V>(lhs: [K : V], rhs: [K : V]) -> [K : V] {
  var combined = lhs
  
  for (k, v) in rhs {
    combined[k] = v
  }
  
  return combined
}
