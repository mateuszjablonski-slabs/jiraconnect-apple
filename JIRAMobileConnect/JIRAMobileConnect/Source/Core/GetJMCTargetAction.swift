//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

// TODO: Re-align all source to 2 space tabs.
class GetJMCTargeAction: AsyncAction {
  let onComplete: Outcome<JMCTarget>->Void
  
  init(onComplete: Outcome<JMCTarget>->Void) {
    self.onComplete = onComplete
    super.init()
  }
  
  override func run() {
    do {
      let target = try JMCTarget.createTargetFromJSONOnDisk()
      finishedExecutingOperationWithOutcome(.Success(target))
    } catch {
      finishedExecutingOperationWithOutcome(.Error(error))
    }
  }
  
  func finishedExecutingOperationWithOutcome(outcome: Outcome<JMCTarget>) {
    finishedExecutingOperation()
    onComplete(outcome)
  }
}
