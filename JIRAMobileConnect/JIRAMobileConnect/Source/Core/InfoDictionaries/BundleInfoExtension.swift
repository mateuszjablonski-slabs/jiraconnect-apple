//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

extension NSBundle {
  var version: String? {
    return infoDictionary?["CFBundleVersion"] as? String
  }
  
  // TODO: Use long one.
  var versionShort: String? {
    return infoDictionary?["CFBundleShortVersionString"] as? String
  }
  
  var name: String? {
    return infoDictionary?["CFBundleName"] as? String
  }
  
  var displayName: String? {
    return infoDictionary?["CFBundleDisplayName"] as? String
  }
  
  var identifier: String? {
    return bundleIdentifier
  }
  
  var infoAsDictionary: [String: NSObject] {
    return ["appVersion": version ?? "",
      "appVersionShort": versionShort ?? "",
      "appName": name ?? "",
      "appDisplayName": displayName ?? "",
      "appId": identifier ?? ""]
  }
}
