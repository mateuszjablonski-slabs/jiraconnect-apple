//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import UIKit

class NewFeedbackFlowController: UINavigationController {
  let newFeedbackViewController: NewFeedbackViewController
  var leftBarButtonItem: UIBarButtonItem!
  var shouldUseCustomTransition = true
  var originalStatusBarStyle: UIStatusBarStyle?
  var onDidFinish: (Void->Void)?
  
  init(screenshot: UIImage?, settings: FeedbackSettings? = nil) {
    newFeedbackViewController = NewFeedbackFlowViewControllerFactory().createNewFeedbackViewControllerWithScreenshotImage(screenshot, settings: settings)
    super.init(nibName: nil, bundle: nil)
    newFeedbackViewController.onDidFinish = {
      self.dismissFeedbackFlow()
    }
    viewControllers = [newFeedbackViewController]
    
    leftBarButtonItem = self.createCancelBarButtonItem()
    newFeedbackViewController.navigationItem.leftBarButtonItem = leftBarButtonItem
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) is not supported by NewFeedbackFlowController")
  }
}

extension NewFeedbackFlowController {
  // MARK: View Lifecycle & UIKit
  
  override func viewDidLoad() {
    super.viewDidLoad()
    transitioningDelegate = self
    style()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    applyStatusBarStyleIfNeeded()
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    returnStatusBarStyleToOriginalValueIfNeeded()
  }

  // Feedback flow considered finished when this view controller's view has disappeared.
  override func viewDidDisappear(animated: Bool) {
    super.viewDidDisappear(animated)
    self.onDidFinish?()
  }
  
  override func preferredStatusBarStyle() -> UIStatusBarStyle {
    return .LightContent
  }
  
  func applyStatusBarStyleIfNeeded() {
    if NSBundle.mainBundle().viewControllerBasedStatusBarAppearance == false {
      originalStatusBarStyle = UIApplication.sharedApplication().statusBarStyle
      UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
  }
  
  func returnStatusBarStyleToOriginalValueIfNeeded() {
    if NSBundle.mainBundle().viewControllerBasedStatusBarAppearance == false {
      if let originalStatusBarStyle = originalStatusBarStyle {
        UIApplication.sharedApplication().statusBarStyle = originalStatusBarStyle
      }
    }
  }
}

extension NewFeedbackFlowController {
  // MARK: User Actions
  
  func cancel() {
    newFeedbackViewController.cancel() // TODO: Cancelable protocol
    dismissFeedbackFlow()
  }
  
  func presentKeyboard() {
    newFeedbackViewController.presentKeyboard()
  }

  func dismissFeedbackFlow() {
    if let actionController = self.presentingViewController as? UIAlertController {
      if let presentingViewController = actionController.presentingViewController {
        actionController.view.hidden = true
        presentingViewController.dismissViewControllerAnimated(false) {
          actionController.view.hidden = false
          presentingViewController.presentViewController(actionController, animated: true, completion: nil)
        }
        return
      }
    }
    presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
  }
}

extension NewFeedbackFlowController {
  // MARK: Visual Design
  
  func style() {
    navigationBar.style()
    view.backgroundColor = ADGChromeColor
  }
}

extension NewFeedbackFlowController {
  // MARK: Factories
  
  func createCancelBarButtonItem() -> UIBarButtonItem {
    return UIBarButtonItem(title: "global_cancel_button_title".localizedString, style: .Plain, target: self, action: #selector(NewFeedbackViewController.cancel))
  }
}

extension NewFeedbackFlowController: UIViewControllerTransitioningDelegate {
  // MARK: Custom Transition Presentation
  
  func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController,
    sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
      if shouldUseCustomTransition {
        return NewFeedbackFlowPresentationAnimator()
      } else {
        return nil
      }      
  }
}

private class NewFeedbackFlowViewControllerFactory {
  let flowStoryboard = UIStoryboard(name: "NewFeedbackFlow", bundle: Bundle.JIRAMobileConnectBundle)
  
  func createNewFeedbackViewControllerWithScreenshotImage(screenshotImage: UIImage?, settings: FeedbackSettings? = nil) -> NewFeedbackViewController {
    let id = "composeFeedbackViewController"
    let vc = flowStoryboard.instantiateViewControllerWithIdentifier(id) as! NewFeedbackViewController
    vc.screenshotImage = screenshotImage
    vc.settings = try! settings ?? FeedbackSettings() // TODO: don't try!
    return vc
  }
}

extension NSBundle {
  private var viewControllerBasedStatusBarAppearance: Bool {
    return (infoDictionary?["UIViewControllerBasedStatusBarAppearance"] as? Bool) ?? true
  }
}
