//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import Foundation

extension UINavigationBar {
  func style() {
    translucent = false
    barTintColor = ADGNavigationBarColor
    tintColor = ADGNavigationBarTintColor
    titleTextAttributes = [NSForegroundColorAttributeName: ADGNavigationBarTintColor]
  }
}
