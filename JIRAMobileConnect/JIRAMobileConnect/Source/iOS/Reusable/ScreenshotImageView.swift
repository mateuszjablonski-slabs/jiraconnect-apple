//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import UIKit

class ScreenshotImageView: UIImageView {
  var fixedWidth: CGFloat? = 0 {
    didSet {
      invalidateIntrinsicContentSize()
    }
  }
  
  override func intrinsicContentSize() -> CGSize {
    var size = super.intrinsicContentSize()
    
    guard let fixedWidth = fixedWidth else {
      return size
    }
    
    size.height = size.height/size.width * fixedWidth
    size.width = fixedWidth
    return size
  }
  
  func originalIntrinsicContentSize() -> CGSize {
    return super.intrinsicContentSize()
  }
}
