//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

import UIKit

class LayoutConstraintHairline: NSLayoutConstraint {
  override func awakeFromNib() {
    super.awakeFromNib()
    if constant == 1 {
      constant = 1 / UIScreen.mainScreen().scale
    }
  }
}
