//  Copyright © 2016 Atlassian Pty Ltd. All rights reserved.

import UIKit

class SendingIndicatorView: UIView {
  @IBOutlet private weak var sendingIndicatorLabel: UILabel!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    applyStyle()
  }
  
  func applyStyle() {
    sendingIndicatorLabel.textColor = ADGGray
  }
  
  func indicateSending() {
    sendingIndicatorLabel.text = "global_sending_indication_title".localizedString
    activityIndicator.startAnimating()
  }
  
  func indicateSuccess() {
    activityIndicator.stopAnimating()
    sendingIndicatorLabel.text = "global_success_indication_title".localizedString
  }
  
  func indicateError(errorMessage errorMessage: String) {
    activityIndicator.stopAnimating()
    sendingIndicatorLabel.text = errorMessage
  }

}
