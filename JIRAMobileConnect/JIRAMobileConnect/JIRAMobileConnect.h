//  Copyright © 2015 Atlassian Pty Ltd. All rights reserved.

#import <UIKit/UIKit.h>

//! Project version number for JIRAMobileConnect.
FOUNDATION_EXPORT double JIRAMobileConnectVersionNumber;

//! Project version string for JIRAMobileConnect.
FOUNDATION_EXPORT const unsigned char JIRAMobileConnectVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JIRAMobileConnect/PublicHeader.h>
